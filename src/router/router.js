import {createRouter, createWebHistory} from "vue-router";
import Registration from "@/pages/Registration.vue";
import Login from "@/pages/Login.vue";
import ForgotPasswordForm from "@/pages/ForgotPasswordForm.vue";
import ResetPasswordForm from "@/pages/ResetPasswordForm.vue";
import CheckYourEmail from "@/pages/CheckYourEmail.vue";
import MainPage from "@/pages/MainPage.vue";
import ListEvents from "@/pages/ListEvents.vue";
import ListPlaces from "@/pages/ListPlaces.vue";
import NewPasswordApprove from "@/pages/NewPasswordApprove.vue";
import RegistrationApprove from "@/pages/RegistrationApprove.vue";
import EventPage from "@/pages/EventPage.vue";
import PlacePage from "@/pages/PlacePage.vue";
import ProfileDetailsPage from "@/components/ProfileDetails.vue";
import ProfilePage from "@/pages/ProfilePage.vue";
import EventCreate from "@/pages/EventCreate.vue";
import store from "@/store/index.js";


const routes = [
    {
        path: "/",
        name: "MainPage",
        component: MainPage
    },
    {
        path: "/events",
        name: "ListEvents",
        component: ListEvents
    },
    {
        path: "/places",
        name: "ListPlaces",
        component: ListPlaces
    },
    {
        path: "/users/registration",
        name: "Registration",
        component: Registration
    },
    {
        path: "/users/email_approve/:email",
        name: "CheckYourEmail",
        component: CheckYourEmail
    },
    {
        path: "/users/reset_approve/:email",
        name: "ResetPass",
        component: CheckYourEmail
    },
    {
        path: "/login",
        name: "Login",
        component: Login
    },
    {
        path: "/users/password/reset",
        name: "ForgotPasswordForm",
        component: ForgotPasswordForm
    },
    {
        path: "/users/reset-password",
        name: "ResetPasswordForm",
        component: ResetPasswordForm
    },
    {
        path: "/users/registration_approve",
        name: "RegistrationApprove",
        component: RegistrationApprove
    },
    {
        path: "/users/reset-password/approve",
        name: 'NewPasswordApprove',
        component: NewPasswordApprove
    },
    {
        path: '/events/:id',
        name: 'EventDetails',
        component: EventPage
    },
    {
        path: '/places/:id',
        name: 'PlaceDetails',
        component: PlacePage
    },
    {
        path: '/profile',
        name: 'ProfilePage',
        component: ProfilePage,
        meta: {
            requiresAuth: true
        },
    },
    {
        path: '/profile_details',
        name: 'ProfileDetailsPage',
        component: ProfileDetailsPage,
        meta: {
            requiresAuth: true
        },
    },
    {
        path: '/event-create',
        name: 'EventCreate',
        component: EventCreate,
        meta: {
            requiresAuth: true
        },
    }
];

const router = createRouter({
    routes,
    history: createWebHistory()
});


router.beforeEach((to, from, next) => {
    if (to.matched.some(record => record.meta.requiresAuth)) {
        if (!store.state.auth.isAuthenticated) {
            next({
                path: '/login',
                query: {redirect: to.fullPath},
            });
        } else {
            if (to.matched.some(record => record.meta.requiresUser)) {
                if (store.getters["auth/isUser"]) {
                    next();
                } else {
                    next({path: '/'});
                }
            } else {
                next();
            }
        }
    } else {
        next();
    }
});
export default router;