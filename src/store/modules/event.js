import instance from "../../../axios.js";

export default {
    namespaced: true,
    state: {
        createdEventId: null,
        eventsList: [],
        currentEvent: null,
    },
    mutations: {
        setCreatedEventId(state, event) {
            state.createdEventId = event;
        },
        setEventsList(state, events) {
            state.eventsList = events;
        },
        setCurrentEvent(state, event) {
            state.currentEvent = event;
        },
    },
    actions: {
        async getAllEvents({ commit }) {
            try {
                const response = await instance.get('/events');

                console.log("Response:", response);

                if (response.status === 200) {
                    commit('setEventsList', response.data.content);
                    return response.data.content;
                }
            } catch (error) {
                console.error("Error getting events:", error);
                throw error;
            }
        },
        async getEventsByCategory({ commit }, categoryId) {
            try {
                const response = await instance.get(`/events/category/${categoryId}`);
                console.log("Response:", response);

                if (response.status === 200) {
                    commit('setEventsList', response.data);
                    return response.data;
                }
            } catch (error) {
                console.error("Error getting events by category:", error);
                throw error;
            }
        },
        async getEventById({ commit }, eventId) {
            try {
                const response = await instance.get(`/events/${eventId}`);
                console.log("Response:", response);

                if (response.status === 200) {
                    commit('setCurrentEvent', response.data);
                    return response.data;
                }
            } catch (error) {
                console.error("Error getting event by ID:", error);
                throw error;
            }
        },
        async create({ commit ,rootState}, eventData) {
            try {
                const response = await instance.post('/events/create', eventData, {
                    headers: {
                        Authorization: `Bearer ${rootState.auth.token}`,
                    },
                });
                console.log("Response:", response);

                if (response.status === 200) {
                    commit('setCreatedEventId', response.data);
                    return response.data;
                }
            } catch (error) {
                console.error("Error creating event:", error);
                throw error;
            }
        },
        async addedImages({ commit, rootState }, {id,event}) {
            try {
                const response = await instance.post(`/events/create/images/${id}`, event, {
                    headers: {
                        Authorization: `Bearer ${rootState.auth.token}`,
                        'Content-Type': 'multipart/form-data',
                    },
                });

                console.log("Response:", response);

                if (response.status === 201) {
                    commit('setCreatedPlace', response.data);
                    return response.data;
                }
            } catch (error) {
                console.error("Error creating place:", error);
                throw error;
            }
        },
    },
    getters: {
        events: state => state.eventsList,
        currentEvent: state => state.currentEvent,
        getCreatedEventId: state => state.createdEventId,
    },
};
