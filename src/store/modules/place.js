import instance from "../../../axios.js";

export default {
    namespaced: true,
    state: {
        placesList: [],
    },
    mutations: {
        setPlacesList(state, places) {
            state.placesList = places;
        },
    },
    actions: {
        async getPlacesList({commit, rootState}) {
            try {
                const response = await instance.get('places/active', {
                    headers: {
                        Authorization: `Bearer ${rootState.auth.token}`,
                    },
                });

                console.log("Places List Response:", response);

                if (response.status === 200) {
                    commit('setPlacesList', response.data);
                    return response.data;
                }
            } catch (error) {
                console.error("Error fetching places list:", error);
                throw error;
            }
        },
        async searchPlaces({ commit, rootState }, searchParams) {
            try {
                let title = null;
                let address = null;

                const trimmedQuery = searchParams.query.trim();

                if (trimmedQuery.includes(',')) {
                    address = trimmedQuery;
                } else {
                    title = trimmedQuery;
                }

                const response = await instance.post('places/search', {
                    title,
                    address,
                }, {
                    headers: {
                        Authorization: `Bearer ${rootState.auth.token}`,
                    },
                });

                console.log("Search Places Response:", response);

                if (response.status === 200) {
                    commit('setPlacesList', response.data);
                    return response.data;
                }
            } catch (error) {
                console.error("Error searching places:", error);
                throw error;
            }
        },
    },
getters: {
        places: state => state.placesList,
    },
};
