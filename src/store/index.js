import { createStore } from 'vuex';
import event from '@/store/modules/event.js';
import auth from "@/store/modules/auth.js";
import category from "@/store/modules/category.js";
import city from "@/store/modules/city.js";
import place from "@/store/modules/place.js";
import tag from "@/store/modules/tag.js";
import user from "@/store/modules/user.js";

export default createStore({
    modules: {
        auth,
        event,
        category,
        city,
        place,
        tag,
        user,
    },
});