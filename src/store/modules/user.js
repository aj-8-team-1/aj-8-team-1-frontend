import instance from "../../../axios.js";

export default {
    namespaced: true,
    state: {
        userProfile: [],
        userProfileDetail: [],
        userImage: '',
    },
    mutations: {
        setUserProfile(state, profile) {
            state.userProfile = profile;
        },
        setUserProfileDetail(state, profile) {
            state.userProfileDetail = profile;
        },
        setUserImage(state, image) {
            state.userImage = image;
        },
    },
    actions: {
        async getUserProfile({ commit, rootState }) {
            try {
                const response = await instance.get('/users/profile', {
                    headers: {
                        Authorization: `Bearer ${rootState.auth.token}`,
                    },
                });

                console.log("User Profile Response:", response);

                if (response.status === 200) {
                    commit('setUserProfile', response.data);
                    return response.data;
                }
            } catch (error) {
                console.error("Error fetching user profile:", error);
                throw error;
            }
        },
        async getUserProfileDetail({ commit, rootState }) {
            try {
                const response = await instance.get('/users/profile/detail', {
                    headers: {
                        Authorization: `Bearer ${rootState.auth.token}`,
                    },
                });

                console.log("User Profile Detail Response:", response);

                if (response.status === 200) {
                    const image = response.data.image;
                    commit('setUserProfileDetail', response.data)
                    commit('setUserImage', image);
                    return image;
                }
            } catch (error) {
                console.error("Error fetching user profile detail:", error);
                throw error;
            }
        },
        async updateUserProfileImage({ commit, rootState }, imageFile) {
            try {
                const formData = new FormData();
                formData.append('file', imageFile);

                const response = await instance.post('/users/profile/detail/image', formData, {
                    headers: {
                        'Content-Type': 'multipart/form-data',
                        Authorization: `Bearer ${rootState.auth.token}`,
                    },
                });

                if (response.status === 200) {
                    console.log("Profile image updated successfully");
                }
            } catch (error) {
                console.error("Error updating profile image:", error);
                throw error;
            }
        },
    },
    getters: {
        userProfile: state => state.userProfile,
        userImage: state =>state.userImage,
    },
};
