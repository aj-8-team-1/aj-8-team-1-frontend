FROM node:lts-alpine AS builder

# Устанавливаем рабочую директорию
WORKDIR /app

# Копируем package.json и устанавливаем зависимости
COPY package*.json ./
RUN npm install

# Копируем остальные файлы и собираем приложение
COPY . .

ARG VITE_BASE_URL
RUN echo "VITE_BASE_URL:${VITE_BASE_URL}"
RUN echo "VITE_BASE_URL${VITE_BASE_URL}" >> .env

ARG MINIO_BASE_URL
RUN echo "MINIO_BASE_URL:${MINIO_BASE_URL}"
RUN echo "MINIO_BASE_URL${MINIO_BASE_URL}" >> .env

RUN npm run build

FROM nginx:1.21-alpine

COPY --from=builder /app/dist /usr/share/nginx/html
COPY nginx.conf /etc/nginx/conf.d/default.conf

CMD ["nginx", "-g", "daemon off;"]