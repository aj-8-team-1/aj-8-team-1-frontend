import instance from "../../../axios.js";

export default {
    namespaced: true,
    state: {
        categoryList: [],
    },
    mutations: {
        setCategoryList(state, category) {
            state.categoryList = category;
        },
    },
    actions: {
        async getCategoryList({commit, rootState}) {
            try {
                const response = await instance.get('categories', {
                    headers: {
                        Authorization: `Bearer ${rootState.auth.token}`,
                    },
                });

                console.log("Categories List Response:", response);

                if (response.status === 200) {
                    commit('setCategoryList', response.data);
                    return response.data;
                }
            } catch (error) {
                console.error("Error fetching categories list:", error);
                throw error;
            }
        },
    },
    getters: {
        categories: state => state.categoryList,
    },
};
