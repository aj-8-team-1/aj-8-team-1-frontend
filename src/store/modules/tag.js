import instance from "../../../axios.js";

export default {
    namespaced: true,
    state: {
        tagList: [],
    },
    mutations: {
        setTagList(state, tag) {
            state.tagList = tag;
        },
    },
    actions: {
        async getList({commit, rootState}) {
            try {
                const response = await instance.get('tags/active', {
                    headers: {
                        Authorization: `Bearer ${rootState.auth.token}`,
                    },
                });

                console.log("Tag List Response:", response);

                if (response.status === 200) {
                    commit('setTagList', response.data);
                    return response.data;
                }
            } catch (error) {
                console.error("Error fetching tags list:", error);
                throw error;
            }
        },
    },
    getters: {
        tags: state => state.tagList,
    },
};
