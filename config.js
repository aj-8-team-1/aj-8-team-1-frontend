const minioUrl = import.meta.env.MINIO_BASE_URL;
const imagesBaseUrl = `${minioUrl}/images`;

export { imagesBaseUrl };