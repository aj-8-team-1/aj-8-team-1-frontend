import instance from "../../../axios.js";

export default {
    namespaced: true,
    state: {
        cityList: [],
    },
    mutations: {
        setCityList(state, city) {
            state.cityList = city;
        },
    },
    actions: {
        async getList({commit, rootState}) {
            try {
                const response = await instance.get('cities', {
                    headers: {
                        Authorization: `Bearer ${rootState.auth.token}`,
                    },
                });

                console.log("City List Response:", response);

                if (response.status === 200) {
                    commit('setCityList', response.data);
                    return response.data;
                }
            } catch (error) {
                console.error("Error fetching city list:", error);
                throw error;
            }
        },
    },
    getters: {
        cities: state => state.cityList,
    },
};
