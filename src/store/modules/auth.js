import instance from "../../../axios.js";

const TOKEN_KEY = 'accessToken';

export default {
    namespaced: true,
    state: {
        token: localStorage.getItem(TOKEN_KEY) || null,
        isAuthenticated: Boolean(localStorage.getItem(TOKEN_KEY)),
        userData: JSON.parse(localStorage.getItem('userData')) || null,
        incorrectPassword: false,
    },
    mutations: {
        setToken(state, token) {
            state.token = token;
            localStorage.setItem(TOKEN_KEY, token);

            const decodedToken = parseJwt(token);

            localStorage.setItem('userData', JSON.stringify(decodedToken));

            state.userData = decodedToken;
        },
        clearToken(state) {
            state.token = null;
            state.userData = null;
            localStorage.removeItem(TOKEN_KEY);

            localStorage.removeItem('userData');
        },
        setAuthenticated(state, value) {
            state.isAuthenticated = value;
        },
        setIncorrectPassword(state, value) {
            state.incorrectPassword = value;
        },
    },
    actions: {
        async login({commit}, userDTO) {
            try {
                const response = await instance.post('/users/login', userDTO);
                if (response.status === 200) {
                    commit('setToken', response.headers['access_token']);
                    commit('setAuthenticated', true);
                    commit('setIncorrectPassword', false);
                    return true;
                }
            } catch {
                commit('setIncorrectPassword', true);
                return false;
            }
        },
        startTokenExpirationTimer({ commit, state }) {
            const expirationTime = state.userData.exp * 1000;
            const currentTime = Date.now();

            const timerDuration = expirationTime - currentTime;
            setTimeout(() => {
                commit('clearToken');
                commit('setAuthenticated', false);

            }, timerDuration);
        },
    },
    getters: {
        isUser: (state) => state.userData && state.userData.authorities.includes('USER'),
        getSub: (state) => state.userData ? state.userData.sub : null,
    }
};

function parseJwt(token) {
    const base64Url = token.split('.')[1];
    const base64 = base64Url.replace(/-/g, '+').replace(/_/g, '/');
    const jsonPayload = decodeURIComponent(atob(base64).split('').map(function (c) {
        return '%' + ('00' + c.charCodeAt(0).toString(16)).slice(-2);
    }).join(''));

    return JSON.parse(jsonPayload);
}