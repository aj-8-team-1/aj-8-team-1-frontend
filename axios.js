import axios from 'axios';
import store from "@/store/index.js";

const instance = axios.create({
    baseURL: import.meta.env.VITE_BASE_URL,
    timeout: 10000,
    headers: {
        'Content-Type': 'application/json'
    },
});

instance.interceptors.response.use(
    (response) => {
        return response;
    },
    (error) => {
        if (error.response && error.response.status === 403) {
            store.commit('auth/clearToken');
            store.commit('auth/setAuthenticated', false);
        }
        return Promise.reject(error);
    }
);
export default instance;